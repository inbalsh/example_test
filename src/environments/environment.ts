// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {	
  apiKey: "AIzaSyBJUdGuNkU4_DI5DB75WtGxDRZz9WYmunk",
  authDomain: "exampletest-e5fb8.firebaseapp.com",
  databaseURL: "https://exampletest-e5fb8.firebaseio.com",
  projectId: "exampletest-e5fb8",
  storageBucket: "exampletest-e5fb8.appspot.com",
  messagingSenderId: "834662807397"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
