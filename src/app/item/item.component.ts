import { Component, OnInit, Input } from '@angular/core';
import { ItemsService } from '../items.service';

@Component({
  selector: 'item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {

  @Input() data:any; // הגדרת אינפוט עם תכונה דטה

  name;
  price;
  status;
  key;

  showTheButton = false; // אחראי על הצגת כפתורים
  deleteField = false;
  showDelete = true;

  isInInventory:boolean = false; // תכונה לצקבוקס

  showButton(){
    this.showTheButton = true; // אחראי על הצגת כפתורים
    console.log ("works");
  }
  hideButton(){
    this.showTheButton = false; // אחראי על הסתרת כפתורים
    console.log ("works");
  }

  delete(){
    this.deleteField = true; // להציג את שני הכפתורים הבאים
    this.showTheButton = false; // לא להציג כפתור דיליט ראשון
    this.showDelete = false; // לא להציג כפתור דיליט ראשון
    console.log ("works");
  }

  deleteitem(){
    this.itemService.delete(this.key) // מחיקה לגמרי
    this.deleteField = false; // שלא יציג את שני הכפתורים שאחרי דיליט הראשון
  }
  
  cancel(){
    this.deleteField = false; // שלא יציג את שני הכפתורים שאחרי דיליט הראשון
    this.showDelete = true; //שיציג את כפתור דיליט הראשון
   }


   changeInventory(){
    console.log(this.isInInventory);
    this.itemService.updateInevntory(this.key, this.isInInventory);
  }


  constructor(private itemService:ItemsService) { }

  ngOnInit() {
    this.name = this.data.name;
    this.price = this.data.price;
    this.status = this.data.status;
    this.key = this.data.$key; // מעדכנת את הפרמטר קי עם המשתנה שהוכנס
    this.isInInventory = this.data.status;   //

  }

}
