import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';


//material angular
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input'; 
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule} from '@angular/material/button';
import {MatCheckboxModule} from '@angular/material/checkbox';


//firebase modules
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';


import { Routes, RouterModule } from '@angular/router';
import {environment} from '../environments/environment'; // הוספת אימפורט

import { AppComponent } from './app.component';
import { NavComponent } from './nav/nav.component';
import { ItemsComponent } from './items/items.component';
import { ItemComponent } from './item/item.component';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { GreetingsComponent } from './greetings/greetings.component';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    ItemsComponent,
    ItemComponent,
    LoginComponent,
    NotFoundComponent,
    GreetingsComponent
  ],
  imports: [
    BrowserModule,
    MatCardModule,
    AngularFireModule.initializeApp(environment.firebase), //    AngularFireDatabaseModule,
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    FormsModule,
    MatInputModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    RouterModule.forRoot([ // כאן נגדיר נתיב, כל נתיב הינו ג'ייסון
    {path:'', component:ItemsComponent}, //ברירת מחדל
    {path:'item', component:ItemComponent},
    {path:'items', component:ItemsComponent},
    {path:'login', component:LoginComponent},
    {path:'greetings', component:GreetingsComponent},
    {path:'**', component:NotFoundComponent} // אם היוזר מכניס קישור לא מוכר, לכאן זה יגיע

    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
