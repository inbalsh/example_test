import { Injectable } from '@angular/core';
import {AuthService} from './auth.service'; // הוספת אימפורט עם נקודה אחת לפני כי חוזרים תיקייה אחת אחורה ולא שתיים
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database' //הוספה

@Injectable({
  providedIn: 'root'
})
export class ItemsService {


  delete(key){ // הוספת פונקציית מחיקה
    console.log ("workkkkkk");
    this.authService.user.subscribe(user=>{
      this.db.list('/users/'+user.uid+'/items').remove(key);
    })
  }

    // לעדכן צקבוקס בפיירבייס
  updateInevntory(key,isInInventory){
    console.log(key);
    console.log(isInInventory);
    this.authService.user.subscribe(
      user =>{
        let uid = user.uid;
        this.db.list('users/'+uid +'/items').update(key,{'status':isInInventory});
      }
    )   
  }


  constructor(private authService:AuthService,
    private db:AngularFireDatabase,
  ) { }

}
