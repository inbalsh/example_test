import { Component, OnInit } from '@angular/core';
import {AuthService} from '../auth.service'; // הוספת אימפורט
import {Router} from "@angular/router";

@Component({
  selector: 'greetings',
  templateUrl: './greetings.component.html',
  styleUrls: ['./greetings.component.css']
})
export class GreetingsComponent implements OnInit {

  constructor(private router:Router,  public authService:AuthService)
   { }


  ngOnInit() {
  }

}
