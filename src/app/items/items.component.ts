import { Component, OnInit } from '@angular/core';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database' //הוספה
import {AuthService} from '../auth.service'; 

@Component({
  selector: 'items',
  templateUrl: './items.component.html',
  styleUrls: ['./items.component.css']
})
export class ItemsComponent implements OnInit {

  items = [];

  constructor(private db:AngularFireDatabase,
    private authService:AuthService,) { }

  ngOnInit() {
    this.authService.user.subscribe(user => {
      this.db.list('/users/'+user.uid+'/items').snapshotChanges().subscribe( // יצירת אובייקט מסוג אנגולר שנקרא דיבי, לאובייקט קיימת פונקציה שנקראת ליסט אשר קוראת מהדטה בייס רשימה של אובייקטים ומקבלת אנדפוינט בשם טודוס , סנאפשוט מילה שמורה שזו אובסרבבל מוכן לשימוש
        items =>{ // אינפוט איטטמס מפיירבייס
          this.items = []; // מאפסים את כל טודוס, הפיכה למערך ריק. זה הטודוס שלנו שהגדרנו למעלה
          items.forEach( // הפעלת הפונקציה פוראיצ' לכל הטודוס מפיירבייס. זוהי לולאה שרצה על כל האובייקטים ברשימה בפיירבייס - קי 
            item => { // הפונקציה קוראת לכל אחד באופן זמני - טודו
              let y = item.payload.toJSON(); // משתנה חדש בו נכניס את התוכן של קי כלומר הפיילוד והעברה לפורמט גייסון 
              y["$key"] = item.key; // אנו נרצה שוואי יכיל גם את השדה קי ולכן כאן מבצעת הוספת שדה בו נכניס את הטודו נקודה קי -מילה שמורה
              this.items.push(y); // הכנסת האיבר שיצרנו לתוך טודוס
              // לכל טודו יש שני שדות: קי ופיילוד (התוכן). ולכן ניתן לעשות טודו נקודה קי או נקודה פיילוד
            }
          )
        }
      )
    })
  } 

}
